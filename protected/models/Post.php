<?php

/**
 * This is the model class for table "post".
 * @property integer $id
 * @property string  $autor
 * @property string  $text
 * @property string  $date
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string  $alias
 * @property integer $parent_id
 */
class Post extends CActiveRecord{
	
	/**
	 * @var int 
	 */
	public $parent_id;
	
	/**
	 * Behavior
	 * @return array
	 */
	public function behaviors(){
		return array(
			'nestedSetBehavior' => array(
				'class'          => 'application.components.NestedSetBehavior',
				'leftAttribute'  => 'lft',
				'rightAttribute' => 'rgt',
				'levelAttribute' => 'level',
			),
		);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		return array(
			array('lft, rgt, level', 'numerical', 'integerOnly'=>true),
			array('date', 'safe'),
			array('autor, text ', 'required'),
			array('text', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9\.\,\(\)]+$/u', 'message'=> 'Есть некорректные символы!'),
			array('text', 'length', 'max'=>1020),
			array('autor', 'length', 'max'=>100),
			array('id, autor, text, date, lft, rgt, level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id'    => 'ID',
			'autor' => 'Автор',
			'text'  => 'Комментарий',
			'date'  => 'Дата',
			'lft'   => 'Lft',
			'rgt'   => 'Rgt',
			'level' => 'Level',
			'alias' => 'Alias',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('autor',$this->autor,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('lft',$this->lft);
		$criteria->compare('rgt',$this->rgt);
		$criteria->compare('level',$this->level);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Создаем корневой пост или возвращаем уже имеющийся
	 * @param Post $model
	 * @return mixed
	 */
	public static function getRoot(Post $model){
		$dependency = new CDbCacheDependency('SELECT max("date") FROM post');
		$root = $model->roots()->cache(60*60*6, $dependency)->find();
		if (! $root){
			$model->autor = 'root';
			$model->text = 'Root';
			$model->saveNode();
			$root = $model->roots()->find();
		}
		return $root;
	}

}
