<?php

class PostController extends Controller {
	
	/**
	 * @var string 
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
		);
	}

	/**
	 * Создаем пост
	 */
	public function actionCreate(){
		$model = new Post;
		$parent_id = Yii::app()->request->getParam('parent_id',1);
		
		if(isset($_POST['Post'])){
			$model->attributes = $_POST['Post'];
			$current_root = Post::model()->findByPk((int)$_POST['Post']['parent_id']);
			
			if($model->appendTo($current_root)){
				$this->redirect('/');
			}
		}
 
		$this->render('create',array(
			'model'      =>$model,
			'parent_id'  => $parent_id,
			'id'         => null,
		));
	}



	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id){
		$this->loadModel($id)->deleteNode();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Удалить все записи
	 */
	public function actionDelete_all(){
		Yii::app()->db->createCommand('TRUNCATE post')->query();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Все посты
	 */
	public function actionIndex(){
		$model = new Post;
		$root = Post::getRoot($model);
		$dependency = new CDbCacheDependency('SELECT max("date") FROM post');
		$posts = $root->descendants()->cache(60*60*6, $dependency)->findAll();
		$this->render('index',array(
			'root'  => $root,
			'posts' => $posts,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Post the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Post::model()->findByPk($id);
		if($model===null){
			throw new CHttpException(404,'Такой страницы нет.');
		}
		return $model;
	}
}
