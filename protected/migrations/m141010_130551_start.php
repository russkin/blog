<?php

class m141010_130551_start extends CDbMigration {
	
	public function up(){
		$this->execute("CREATE TABLE IF NOT EXISTS `post` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`autor` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Автор',
		`text` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Комментарий',
		`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата',
		`lft` int(11) NOT NULL,
		`rgt` int(11) NOT NULL,
		`level` int(11) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");
	}

	public function down()
	{
		echo "drop table post";
		return false;
	}

}