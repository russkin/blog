<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
	'Posts' => array('index'),
	'Create',
);
?>

<h1>Новый коммент</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'parent_id' => $parent_id)); ?>