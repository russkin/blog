<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'                     => 'post-form',
	'enableClientValidation' => TRUE,
)); ?>
	<?=$form->hiddenField($model, 'parent_id', $htmlOptions=array('value'=>$parent_id)) ?>
	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->labelEx($model,'autor'); ?>
		<?= $form->textField($model,'autor',array('size'=>60,'maxlength'=>200)); ?>
		<?= $form->error($model,'autor'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($model,'text'); ?>
		<?= $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
		<?= $form->error($model,'text'); ?>
	</div>
	<div class="row buttons">
		<?= CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->