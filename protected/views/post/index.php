<div  class="index">
	<?php
	$this->breadcrumbs = array(
		'Посты',
	);
	?>
	<h1>Посты</h1>
	<?= $this->renderPartial('menu')?>
	
	<?php if ($posts): ?>
		<?php $level = 0; ?>
		<?php foreach($posts as $n => $post): ?>
			<?php if ($post->level == $level): ?>
				</li>
			<?php elseif($post->level > $level): ?>
				<ul>
			<?php else: ?>
				</li>
				<?php for($i = $level - $post->level; $i; $i--):?>
					</ul>
					</li>
				<?php endfor;?>
			<?php endif; ?>
			<li>
				<?= $this->renderPartial('view', array('model'=>$post))?>
				<?php
					$level = $post->level;
				?>
		<?php endforeach; ?>
		<?php for ($i = $level; $i; $i--): ?>
			</li>
			</ul>
		<?php endfor; ?>
	<?php else: ?>
		<div>
			А вот нету комментариев ;(
		</div>
	<?php endif; ?>
	<?= $this->renderPartial('menu')?>
</div>