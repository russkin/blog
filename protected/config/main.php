<?php

/**
 * @author Andrew Russkin <andrew.russkin@gmail.com>
 */
return array(
	'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'              => 'Yii Blog ',
	'sourceLanguage'    => 'en',
	'language'          => 'ru',
	// preloading 'log' component
	'preload'           => array('log'),
	// autoloading model and component classes
	'import'            => array(
		'application.models.*',
		'application.components.*',
	),
	
	'defaultController' => 'post',
	'modules'           => array(
	),
	// application components
	'components'        => array(
		'cache' => array(
			'class'       => 'system.caching.CMemCache',
			'servers'     => array(
				array('host' => 'localhost', 'port' => 11211, 'weight' => 100),
			),
			'useMemcached' => false,
		),
		'user'            => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),
		'db'              => require('db.php'),
		'errorHandler'    => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'urlManager'      => array(
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'rules'          => array(
				'post/<id:\d+>/<title:.*?>' => 'post/view',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		'log'             => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => require(dirname(__FILE__) . '/params.php'),
);
