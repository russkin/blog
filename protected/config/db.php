<?php

/**
 * настройки БД
 * @author Andrew Russkin <andrew.russkin@gmail.com>
 */
return array(
	'connectionString'       => 'mysql:host=localhost;dbname=blog',
	'username'               => 'blog',
	'password'               => 'blog',
	'charset'                => 'utf8',
	'emulatePrepare'         => true,
	'enableProfiling'        => true,
	'enableParamLogging'     => true,
	'schemaCachingDuration'  => 3600,
	'queryCacheID'           => 'cache',
	
);

