<?php

/**
 * Удаляем все записи
 * @author Andrew Russkin <andrew.russkin@gmail.com>
 * 
 */


class TruncateCommand extends CConsoleCommand {

	public function run() {
		$data=  date('Y-m-d H:i:s', strtotime('-1 hour'));
		Yii::app()->db->createCommand('delete from post where `date`<:date')->bindParam(':date', $data)->query();
		$hour=  (int)date('h');
		if (ceil($hour/4) == $hour/4){
			$this->_truncate();
		}
	}
	
	private function _truncate(){
		Yii::app()->db->createCommand('TRUNCATE post')->query();
	}
}